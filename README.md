# thirdlove

## Technologies used

React, JS, jQuery, SASS, HTML5, CSS3.

## Libraries and dependencies

I used `create-react-app` to create this standalone page/exercise, but I had to eject it later.
`react-owl-carousel` for the carousel and `jQuery` as a dependency.
`bootstrap` to make the site responsive.

##Google Fonts

    font-family: 'Abhaya Libre', serif;
    font-family: 'Montserrat', sans-serif;
    font-family: 'Open Sans', sans-serif;

## Mocked data source
http://www.mocky.io/v2/5bb4fe093000006600aabc24

## Brief description of what I did

Before the app loads, I get the mocked json data from the URL above. After some light parsing, I store all variations of the product in the app's state.
Each section is its own component, so I load those and pass the relevant props/info to them.

On first load, I check the first available product and load each filter with its data. The filters are the color picker, the band picker and the cup picker.
When the user makes a selection, I look for all available variations with the selected data and reload the filter controls accordingly.

On the images' side, I used owl carousel, since that's the one you're using on the live site. Since it requires jQuery, I load that one as well.
I created the thumbnails myself, as that's something owl carousel doesn't seem to support.