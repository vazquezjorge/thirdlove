import React from 'react';
import ColorPicker from './color_picker/color_picker'

import './product_info.scss'

class ProductInfo extends React.Component {

    state = {
        products: [],
        currentProduct: {
            id: 0,
            stock: 0,
            price: "",
            color: "",
            band: "",
            cup: ""
        },
        
        // This will contain each color a variation of the current product has.
        // It will be used to populate the color picker.
        uniqueColors: []
    };

    constructor(props) {
        super(props);
                
        this.priceRef = React.createRef();
        this.colorRef = React.createRef();
        this.stockRef = React.createRef();
        this.bandRef = React.createRef();
        this.cupRef = React.createRef();
    }


    componentWillMount() {
        let products = [];

        // Iterate over all variations and parse the data.
        // Convert it into something readable and filterable.
        // eslint-disable-next-line
        Array.from(this.props.variants).map( (element, index) => {
            
            // Check if it has all its properties (some products came without 'price' from the json data)
            if(element.id && element.price && element.option1 && element.option2 && (element.inventory_quantity !== undefined && element.inventory_quantity != null )) {

                products.push({
                    id: element.id,
                    stock: element.inventory_quantity,
                    price: element.price,
                    color: element.option1,
                    band: element.option2.substring(0, 2),
                    cup: element.option2.substring(2)
                });
            }
        });

        this.setState({products: products}, function () {
            this.populateWithFirstVariation();
        });
    }


    // This method will find the first product from the state and populate all fields.
    populateWithFirstVariation = () => {
        // Get the first product
        let firstProduct = this.state.products[0];
        this.setState({currentProduct: firstProduct}, () => {
            
            // Get all colors from the JSON data
            this.setState({uniqueColors: [...new Set(this.state.products.map(item => item.color))]});
            
            
            // Filter and populate the fields.
            this.populateBand(firstProduct.color);
        });
    }


    // Start the process of filtering the products via the selected data.
    // Get the current variations that have this specified color
    // so we can populate the band field
    populateBand = (color) => {
        let uniqueBands = [];

        // Get all variations of that product in this specific color
        let allVariationsThatMatch = this.state.products.filter(product => product.color === color);

        // Get all bands' unique values
        uniqueBands = [...new Set(allVariationsThatMatch.map(item => item.band))];

        this.bandRef.current.innerHTML = "";

        // Iterate all bands and add them to the select tag
        uniqueBands.forEach((element) => {
            let option = document.createElement("option");
            option.text = element;
            option.value = element;
            if(element === allVariationsThatMatch[0].band) {
                option.selected = "selected";
            }
            this.bandRef.current.add(option);
        });
        
        this.populateCup(color, allVariationsThatMatch[0].band);
        
    }

    // Get the current variations that have this specified color and band
    // so we can populate the cup field
    populateCup = (color, band) => {
        let uniqueCups = [];

        // Get all variations of that product in this specific band & color
        let allVariationsThatMatch = this.state.products.filter(product => {return product.color === color && product.band === band});
        
        // Get all cups' unique values
        uniqueCups = [...new Set(allVariationsThatMatch.map(item => item.cup))];

        this.cupRef.current.innerHTML = "";

        if (allVariationsThatMatch !== undefined && allVariationsThatMatch.length !== 0) {

            // Iterate all cups and add them to the select tag
            uniqueCups.forEach((element) => {
                let option = document.createElement("option");
                option.text = element;
                option.value = element;
                if(element === this.state.currentProduct.cup) {
                    option.selected = "selected";
                }
                this.cupRef.current.add(option);
            });

            //Set the current product as the first variation of the found products.
            this.setState({currentProduct: allVariationsThatMatch[0]});
        }
    }



    /*
    * 
    *   Event Handlers
    * 
    */
    addToBag = () => {
        alert(`Added a ${this.props.title} - ${this.state.currentProduct.band}${this.state.currentProduct.cup} to the cart`);
    }

    // Color picker 
    colorChangeHandler = (color) => {
        this.populateBand(color);
    }

    bandChangeHandler = () => {
        let color = this.state.currentProduct.color;
        let newBand = this.bandRef.current.options[this.bandRef.current.selectedIndex].value;

        this.populateCup(color, newBand);

        let found = this.state.products.find((element) => {
            return (element.band === newBand && element.color === color);
        });

        if(found) {
            this.setState({currentProduct: found});
        }
    }

    cupChangeHandler = () => {
        let color = this.state.currentProduct.color;
        let newBand = this.bandRef.current.options[this.bandRef.current.selectedIndex].value;
        let newCup = this.cupRef.current.options[this.cupRef.current.selectedIndex].value;

        let found = this.state.products.find((element) => {
            return (element.cup === newCup && element.band === newBand && element.color === color);
        });

        if(found) {
            this.setState({currentProduct: found});
        }
    }

    /*
    *
    *   Render
    *
    */
    render() {
        const {title} = this.props;

        return (
            <div>
                <div className="product_title_block">
                    <h1 className="product_title">{title}</h1>
                
                    <h3 className="product_price" ref={this.priceRef}>${this.state.currentProduct.price}</h3>
                </div>
                
                <div className="product_text">
                COLOR: <span className="product_text_bold" ref={this.colorRef}>{this.state.currentProduct.color}</span>
                </div>
                
                <ColorPicker colors={this.state.uniqueColors} selected={this.state.currentProduct.color} updateHandler={this.colorChangeHandler} />
                                
                <div className="product_text">
                STOCK: <span className="product_text_bold" ref={this.stockRef}>{this.state.currentProduct.stock}</span>
                </div>
                
                <div className="product_text product_variations">
                <div className="row">
                    <div className="col-6">
                    <label> BAND SIZE
                        <select ref={this.bandRef} onChange={this.bandChangeHandler}>
                        </select>
                    </label>
                    </div>
                    <div className="col-6">
                    <label> CUP SIZE
                        <select ref={this.cupRef} onChange={this.cupChangeHandler}>
                        </select>
                    </label>
                    </div>
                </div>
                </div>

                <div className="product_button">
                <button onClick={this.addToBag}>ADD TO BAG</button>
                </div>
            </div>
        );
    }

}

export default ProductInfo;