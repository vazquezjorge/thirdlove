import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import './carousel.scss'

class Carousel extends React.Component {

    constructor(props) {
        super(props);
        this.owlRef = React.createRef();
    }

    moveToSlide = (slide) => {
        this.owlRef.current.to(slide,400);
    }

    createSlides = (images) => {
        return Array.from(images).map(function(element, index){
            return <div className="item" key={index}><img src={element.srcMaster} alt="" /></div>;
        })
    }

    createThumbs(images) {
        return Array.from(images).map( (element, index) => {
            return <img src={element.src150} key={index} alt="" onClick={(e) => this.moveToSlide(index, e)} />;
        });
    }

   

    render() {
        const {images} = this.props;

        return (
            <div className="container-fluid carousel_container">
                <div className="row no-gutters">
                    <div className="col-2 d-none d-lg-block carousel_thumb_container">
                        {this.createThumbs(images)}
                    </div>
                    <div className="col-xs col-lg-10">
                        <OwlCarousel ref={this.owlRef} className="owl-theme carousel_image" loop margin={10} items="1">
                            {this.createSlides(images)}
                        </OwlCarousel>
                    </div>
                </div>
            </div>
        );
    }

}

export default Carousel;